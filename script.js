class book {
    constructor(title, author, nbPages, read) {
        this.title = title;
        this.author = author;
        this.nbPages = nbPages;
        this.read = read;
    }
}

let myLibrary = [
  new book("t1", "a1", 200, true),
  new book("t2", "a2", 300, false),
  new book("t3", "a3", 10, true),
];

function addBookToLibrary (b) {
    myLibrary.push(b);
}

function removeBookFromLibrary(title,author) {
     myLibrary = myLibrary.filter(function (value, index, arr) {
       return value.title !== title && value.author !== author;
     });
}

function changeRead(b){
    b.read = !b.read
}


const container = document.querySelector(".container");
const bookGrid = document.querySelector(".bookGrid");

function displayBooks (){
    for (const b of myLibrary){
        displayNewBook(b)
    } 
}

function displayNewBook(b){
    const divBook = document.createElement("div");
    divBook.classList.add("book");
    const title = document.createElement("div");
    title.textContent = b.title;
    title.classList.add("title");
    const author = document.createElement("div");
    author.classList.add("author");
    author.textContent = b.author;
    const nbPages = document.createElement("div");
    nbPages.classList.add("nbPages");
    nbPages.textContent = b.nbPages;
    const readBtn = document.createElement("button");
    readBtn.classList.add("read");
    b.read ? ((readBtn.textContent = "Completed"), readBtn.classList.add("completed")) : ((readBtn.textContent = "Reading"),readBtn.classList.add("reading"));
    readBtn.addEventListener("click",(e)=>{
        changeRead(b);
        b.read
          ? ((readBtn.textContent = "Completed"),
            readBtn.classList.remove("reading"),
            readBtn.classList.add("completed"))
          : ((readBtn.textContent = "Reading"),
            readBtn.classList.remove("completed"),
            readBtn.classList.add("reading"));
    })
    const removeBtn = document.createElement("button");
    removeBtn.textContent = "Remove book";
    removeBtn.addEventListener("click", (e) => {
        const dataSet = e.target.parentNode.dataset;
      removeBookFromLibrary(dataSet.title,dataSet.author);
      bookGrid.innerHTML='';
      displayBooks();
    });
    removeBtn.classList.add("removeButton");
    divBook.appendChild(title);
    divBook.appendChild(author);
    divBook.appendChild(nbPages);
    divBook.appendChild(readBtn);
    divBook.appendChild(removeBtn);
    divBook.dataset.title = b.title;
    divBook.dataset.author = b.author;
    bookGrid.appendChild(divBook);
}

function newBookFromForm(){
    const title = document.getElementById("title").value;
    const author = document.getElementById("author").value;
    const nbPages = document.getElementById("nbPages").value;
    const read = document.getElementById("read").checked;
    return new book(title,author,nbPages,read)
}

const grayBg = document.querySelector(".graybg");
const bookFormDiv = document.querySelector(".addBookDiv");

// const submitBtn = document.getElementById("submitAddBook");
const bookForm = document.querySelector(".addBookForm");


bookForm.addEventListener("submit",e => {
    e.preventDefault();
    console.log("ici");
    const b = newBookFromForm();
    addBookToLibrary(b);
    grayBg.classList.remove("active");
    bookFormDiv.classList.remove("active");
    displayNewBook(b);
})


grayBg.addEventListener("click",()=>{
    grayBg.classList.remove("active");
    bookFormDiv.classList.remove("active");
})

const addBookBtn = document.getElementById("addBookButton");

addBookBtn.addEventListener("click",() =>{
    bookForm.reset();
    grayBg.classList.add("active");
    bookFormDiv.classList.add("active");
})




displayBooks();
